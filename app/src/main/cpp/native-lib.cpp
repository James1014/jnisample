//
// Created by Chih Sheng Cheng on 2020/11/5.
//

#include <jni.h>

extern "C" JNIEXPORT jstring JNICALL
Java_com_example_jnisample_MainActivity_getKeyFromJni(JNIEnv *env, jobject) {
    return env->NewStringUTF("I'm key");
}